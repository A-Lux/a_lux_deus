var sidebar = document.querySelectorAll('.side-bar li');
var content = document.querySelectorAll('.box-content');
for (const i of sidebar) {
    i.addEventListener('click', function (event) {
        event.preventDefault();
        for (const j of sidebar) {
            j.classList.remove('side-bar__item-active');
            for (const q of content) {
                q.classList.remove('box-content__active')
            }
        }
        i.classList.add('side-bar__item-active');
        var attr = i.querySelector('a').getAttribute('href');
        document.getElementById(attr.slice(1)).classList.add('box-content__active');
    });
}

var searchTab = document.querySelectorAll('.fast-search-tabs li a');
var contentFaq = document.querySelectorAll('.faq-content');
for (const i of searchTab) {
    i.addEventListener('click', function (event) {
        event.preventDefault();
        for (const j of searchTab) {
            j.classList.remove('fast-search-tab-active');
            for (const q of contentFaq) {
                q.classList.remove('faq-content-active');
            }
        }
        i.classList.add('fast-search-tab-active');
        var attr = i.getAttribute('href');
        document.getElementById(attr.slice(1)).classList.add('faq-content-active')
    });
}

var faqContentTab = document.querySelectorAll('.faq-content-tabs li a');
var circle = document.querySelectorAll('.faq-content-tabs li a span');
for (const i of faqContentTab) {
    i.addEventListener('click', function (event) {
        event.preventDefault();
        for(const j of circle){
            j.classList.remove('faq-content-active-span');
        }
        i.find('span').add('faq-content-active-span');
        var attr = i.getAttribute('href');

    });
}


// personal-area-tab
$(document).ready(function(){
    $('.user-tab ul li').click(function(){
        var tab = $(this).attr('data-tab');


    $('.user-tab ul li').removeClass('user-tab-active');
    $(this).addClass('user-tab-active');

    $('.user-tab-1').removeClass('user-block-active');
    $('#' + tab).addClass('user-block-active');
    });


    $('.voucher-active').click(function(){
       $('.voucher-active img').attr('src', 'images/voucher-active.png');

    })


})
// personal-area-tab


// slider-tour
 $('.slick-Ban').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  asNavFor: '.slick-Nav',
  responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }

  ]
});

$('.slick-Nav').slick({
  slidesToShow: 6,
  slidesToScroll: 1,
  asNavFor: '.slick-Ban',
  arrows: true,
  focusOnSelect: true,
  responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }

  ]
});
//slider latest seen
$('.latest-seen').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }

  ]
});

$('.slick-Content[data-id=1]').show();

$('.slick-Ban').on('afterChange', function(event, slick, currentSlide){
  $('.slick-Content').hide();
  $('.slick-Content[data-id=' + (currentSlide + 1) + ']').show();
});

(function($) {
    $('.img').click(function () {
        $('.search-area input').show();
    });
    $('.add-fav').hover(function () {
        $('.add-fav').toggleClass('wow animated heartBeat');
    });
    $('.add-comp').hover(function () {
        $('.add-comp').toggleClass('wow animated heartBeat');
    });
    $('.close-menu').click(function (e) {
        e.preventDefault();
        $('.menu').hide();
    });
    $(document).on("click", function(event){
        var $trigger = $(".toggle-menu");
        if($trigger !== event.target && !$trigger.has(event.target).length){
            $(".menu").slideUp("fast");
        }            
    });
    $('.toggle-menu').click(function (e) {
        e.preventDefault();
        $('.menu').slideDown(); 
    });
    $('.show-all').click(function () {
        $('.tour-item,.hot-tour-item, .cruise-item, .made-kz-item, .promo-item, .review-item, .exclusive-tour-item').addClass('wow animated fadeIn').show();
    });
    $('.tour').click(function () {
        $('.tour-item').addClass('wow animated fadeIn').show();
        $('.hot-tour-item, .cruise-item, .made-kz-item, .promo-item, .review-item, .exclusive-tour-item').hide();
    });
    $('.hot-tour').click(function () {
        $('.hot-tour-item').addClass('wow animated fadeIn').show();
        $('.tour-item, .cruise-item, .made-kz-item, .promo-item, .review-item, .exclusive-tour-item').hide();
    });
    $('.cruise').click(function () {
        $('.cruise-item').addClass('wow animated fadeIn').show();
        $('.tour-item, .hot-tour-item, .made-kz-item, .promo-item, .review-item, .exclusive-tour-item').hide();
    });
    $('.made-kz').click(function () {
        $('.made-kz-item').addClass('wow animated fadeIn').show();
        $('.tour-item, .hot-tour-item, .cruise-item, .promo-item, .review-item, .exclusive-tour-item').hide();
    });
    $('.promo').click(function () {
        $('.promo-item').addClass('wow animated fadeIn').show();
        $('.tour-item, .hot-tour-item, .cruise-item, .made-kz-item, .review-item, .exclusive-tour-item').hide();
    });
    $('.reviews').click(function () {
        $('.review-item').addClass('wow animated fadeIn').show();
        $('.tour-item, .hot-tour-item, .cruise-item, .made-kz-item, .promo-item, .exclusive-tour-item').hide();
    });
    $('.exclusive-tour').click(function () {
        $('.exclusive-tour-item').addClass('wow animated fadeIn').show();
        $('.tour-item, .hot-tour-item, .cruise-item, .made-kz-item, .promo-item, .review-item').hide();
    });
    new WOW().init();
})(jQuery);
$(document).mouseup(function(e)
{
    var container = $(".search-area input");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        container.hide(0);
    }
});
$(document).ready(function(){
  if ($('#back-to-top').length) {
    var scrollTrigger = 100, // px
        backToTop = function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('#back-to-top').addClass('show');
            } else {
                $('#back-to-top').removeClass('show');
            }
        };
    backToTop();
    $(window).on('scroll', function () {
        backToTop();
    });
    $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
}
});
/*********************************************************** MENU **********************************************************************/
window.onscroll = function() {myFunction()};

var header = document.getElementById("myTabContent");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
};




/*******************************************************************************************************************************************/
var barChartData = {
    labels: ["5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07","5.07"],
    datasets: [{
        backgroundColor: '#33b5e5',
        data: [
          150,
          200,
          100,
          80,
          250,
          200,
          180,
          120,
          220,

        ]
    }]

};

var ctx1 = document.getElementById("barcanvas").getContext("2d");

window.myBar = new Chart(ctx1, {
    type: 'bar',
    data: barChartData,
    maintainAspectRatio: false,
    options: {
        tooltips: {
            mode: 'index',
            intersect: false
        },
        legend: {
          display: false
        },
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            xAxes: [{
                style: {
                    stacked: true,
                },
                gridLines: {
                display:false
            },
            ticks: {
              fontColor: "#5ea9db"
            }

            }],
            yAxes: [{
                stacked: true,
                ticks: {
                  stepSize: 150,
                  display: false
                },
                gridLines: {
                display:false,
                drawBorder: false,
            },

            }],


        }
    }
});
$(document).ready(function () {
  var div = document.getElementById("bardiv");
  $('#toright').click(function(){
    div.scrollBy(100, 0);
    $('#toright').addClass('active');
    $('#toleft').removeClass('active');
  });
  $('#toleft').click(function(){
    $('#toright').removeClass('active');
    $('#toleft').addClass('active');
    div.scrollBy(-100, 0);
  });
});

$('.slider-owl').owl
