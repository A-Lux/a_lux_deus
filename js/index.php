<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Travelmarket</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" >
</head>
<body>
<div class="content">
    <header class="header">
        <div class="head-menu">
            <div class="container">
                <div class="row modile-rowss">
                    <div class="col-sm-2">
                        <div class="logo">
                            <a href="index.php">
                                <img src="images/logo.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="phone">
                            <a href="tel:87273337060"><img src="images/phone-ico.png" alt="">3337060</a>
                        </div>
                    </div>
                   <div class="offset-lg-4 col-sm-2 desktop-cabinet">
                       <div class="cabinet">
                           <a href=""><img src="images/4229498-32.png" width="24px">Кабинет туриста <img src="images/drop-down-arrow.png" width="12px"></a>
                       </div>
                   </div>
                    <div class="col-sm-2 desktop-cabinet">
                        <div class="toggle-menu">
                            <a href="">Меню <img src="images/menu.png" alt=""></a>
                        </div>
                    </div>
					
					<div class="mobile-cabinet">
						<div class="row">
						<div class="cabinet">
							<a href=""><img src="images/4229498-32.png" width="24px"></a>
						</div>
						

							<div class="toggle-menu">
								<a href=""><img src="images/menu.png" alt=""></a>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
       <div class="container">
           <div class="menu">
               <div class="user-row">
                   <div class="name">
                       <a href=""><p><img src="images/arrow.png" alt="">Ваше имя</p></a>
                   </div>
                   <div class="avatar">
                       <a href=""><img src="images/avatar.png" alt=""></a>
                   </div>
                   <div class="close-menu">
                       <a href=""><img src="images/menu-arrow.png" alt=""></a>
                   </div>
               </div>
               <ul>
                   <li><p><a href="">Каталог туров</a></p></li>
                   <li><p><a href="">Календарь низких цен</a></p></li>
                   <li><p><a href="">Горящие туры</a></p></li>
                   <li><p><a href="">Акции</a></p></li>
                   <li><p><a href="">О компании</a></p></li>
                   <li><p><a href="">Услуги</a></p></li>
                   <li><p><a href="">Интересное</a></p></li>
                   <li><p><a href="">Отзывы клиентов</a></p></li>
                   <li class="space-top"><a href=""><div class="icon"><img src="images/menu-icon1.png" alt=""></div>+7 771 756 65 88</a></li>
                   <li><a href=""><div class="icon"><img src="images/menu-icon2.png" alt=""></div>@travel.info</a></li>
               </ul>
           </div>
           <div class="main-title">
               <h1>Выбирайте и покупайте туры онлайн</h1>
               <p>по ценам туроператоров или ниже с круглосуточной <br>
                   поддержкой до и после покупки</p>
           </div>
           <div class="pick-tour">
               <button>Подберите мне тур</button>
           </div>
           <ul class="nav nav-tabs" id="myTab" role="tablist">
               <li class="nav-item">
                   <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-plane"></i>ТУР</a>
               </li>
               <li class="nav-item">
                   <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fas fa-fire-alt"></i>ГОРЯЩИЙ ТУР</a>
               </li>
               <li class="nav-item">
                   <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"><i class="fas fa-hotel"></i>ОТЕЛИ</a>
               </li>
           </ul>
           <div class="tab-content" id="myTabContent">
               <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                   <div class="input-row">
                       <div class="input-row-item1">
                           <input type="text" placeholder="Откуда">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item2">
                           <input type="text" placeholder="Куда">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item3">
                           <input type="text" placeholder="Когда">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item4">
                           <input type="text" placeholder="Длительность">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item5">
                           <input type="text" placeholder="Туристы">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item6">
                           <input type="text" placeholder="Кол-во звезд">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="search-area">
<!--                           <input type="text" placeholder="Поиск">-->
                           <div class="img">
                               <img src="images/search.png" alt="">
                           </div>
                       </div>
                   </div>
               </div>
               <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                   <div class="input-row">
                       <div class="input-row-item1">
                           <input type="text" placeholder="Откуда">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item2">
                           <input type="text" placeholder="Куда">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item3">
                           <input type="text" placeholder="Когда">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item4">
                           <input type="text" placeholder="Длительность">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item5">
                           <input type="text" placeholder="Туристы">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item6">
                           <input type="text" placeholder="Кол-во звезд">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="search-area">
                          <!-- <input type="text" placeholder="Поиск"> -->
                           <div class="img">
                               <img src="images/search.png" alt="">
                           </div>
                       </div>
                   </div>
               </div>
			  
               <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                   <div class="input-row">
                       <div class="input-row-item1">
                           <input type="text" placeholder="Откуда">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item2">
                           <input type="text" placeholder="Куда">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item3">
                           <input type="text" placeholder="Когда">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item4">
                           <input type="text" placeholder="Длительность">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item5">
                           <input type="text" placeholder="Туристы">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="input-row-item6">
                           <input type="text" placeholder="Кол-во звезд">
						   <img src="images/drop-down-arrow.png" width="12px">
                       </div>
                       <div class="search-area">
                          <!-- <input type="text" placeholder="Поиск"> -->
                           <div class="img">
                               <img src="images/search.png" alt="">
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
    </header>
    <div class="main">
        <div class="container">
            <div class="blue-title show-all">
                <h1>Каталог туров</h1>
            </div>
            <div class="filter-row">
                <div class="filter-item tour">
                    <div class="image">
                        <img src="images/plane.png" alt="">
                    </div>
                    <p>Тур</p>
                </div>
                <div class="filter-item hot-tour">
                    <div class="image">
                        <img src="images/fire.png" alt="">
                    </div>
                    <p>Горящий тур</p>
                </div>
                <div class="filter-item cruise">
                    <div class="image">
                        <img src="images/anchor.png" alt="">
                    </div>
                    <p>Круиз</p>
                </div>
                <div class="filter-item made-kz">
                    <div class="image">
                        <img src="images/kz.png" alt="">
                    </div>
                    <p>Сделано в KZ</p>
                </div>
                <div class="filter-item promo">
                    <div class="image">
                        <img src="images/percent.png" alt="">
                    </div>
                    <p>Акции</p>
                </div>
                <div class="filter-item reviews">
                    <div class="image">
                        <img src="images/star-ico.png" alt="">
                    </div>
                    <p>Отзывы</p>
                </div>
                <div class="filter-item exclusive-tour">
                    <div class="image">
                        <img src="images/cam-ico.png" alt="">
                    </div>
                    <p>Экскурсионный тур</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="catalog-item promo-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/percent.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img1.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/percent.png" alt="">
                                        </div>
                                        <p>Акции</p>
                                    </div>
                                </div>
                                <div class="add-fav">
                                    <i class="far fa-heart "></i>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button>Посмотреть тур!</button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="red-bg">Акции</span> Отели ОАЭ на "Все Включено"
                                    по акционным ценам! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item exclusive-tour-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/cam-ico.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img4.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/cam-ico.png" alt="">
                                        </div>
                                        <p>Экскурсионный тур</p>
                                    </div>
                                </div>
                                <div class="add-fav">
                                    <i class="far fa-heart "></i>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button>Посмотреть тур!</button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="brown-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>Мальдивы</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item tour-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/plane.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/plane.png" alt="">
                                        </div>
                                        <p>Тур</p>
                                    </div>
                                </div>
                                <div class="add-fav">
                                    <i class="far fa-heart "></i>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button>Посмотреть тур!</button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item cruise-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/anchor.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/anchor.png" alt="">
                                        </div>
                                        <p>Тур</p>
                                    </div>
                                </div>
                                <div class="add-fav">
                                    <i class="far fa-heart "></i>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button>Посмотреть тур!</button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 ">
                    <div class="catalog-item review-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/star-ico.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <img src="images/img2.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/star-ico.png" alt="">
                                        </div>
                                        <p>Отзыв</p>
                                    </div>
                                </div>
                                <div class="add-fav">
                                    <i class="far fa-heart "></i>
                                </div>
                                <div class="rating">
                                    <p>Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button>Посмотреть тур!</button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="purple-bg">Отзыв</span> Отзыв о Бодруме!</p>
                            </div>
                            <div class="country">
                                <p>Греция</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item tour-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/plane.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/plane.png" alt="">
                                        </div>
                                        <p>Тур</p>
                                    </div>
                                </div>
                                <div class="add-fav">
                                    <i class="far fa-heart "></i>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button>Посмотреть тур!</button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item review-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/star-ico.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <img src="images/img2.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/star-ico.png" alt="">
                                        </div>
                                        <p>Отзыв</p>
                                    </div>
                                </div>
                                <div class="add-fav">
                                    <i class="far fa-heart "></i>
                                </div>
                                <div class="rating">
                                    <p>Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button>Посмотреть тур!</button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="purple-bg">Отзыв</span> Отзыв о Бодруме!</p>
                            </div>
                            <div class="country">
                                <p>Греция</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item hot-tour-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/fire.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/fire.png" alt="">
                                        </div>
                                        <p>Горящий  Тур</p>
                                    </div>
                                </div>
                                <div class="add-fav">
                                    <i class="far fa-heart "></i>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button>Посмотреть тур!</button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="orange-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item made-kz-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/kz.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/kz.png" alt="">
                                        </div>
                                        <p>Тур</p>
                                    </div>
                                </div>
                                <div class="add-fav">
                                    <i class="far fa-heart "></i>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button>Посмотреть тур!</button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 ">
                    <div class="catalog-item exclusive-tour-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/cam-ico.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img3.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/cam-ico.png" alt="">
                                        </div>
                                        <p>Экскурсионный тур</p>
                                    </div>
                                </div>
                                <div class="add-fav">
                                    <i class="far fa-heart "></i>
                                </div>
                               <div class="rating">
                                   <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                               </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button>Посмотреть тур!</button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="brown-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item hot-tour-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/fire.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/fire.png" alt="">
                                        </div>
                                        <p>Горящий  Тур</p>
                                    </div>
                                </div>
                                <div class="add-fav">
                                    <i class="far fa-heart "></i>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button>Посмотреть тур!</button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="orange-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item promo-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/percent.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img1.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/percent.png" alt="">
                                        </div>
                                        <p>Акции</p>
                                    </div>
                                </div>
                                <div class="add-fav">
                                    <i class="far fa-heart "></i>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button>Посмотреть тур!</button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="red-bg">Акции</span> Отели ОАЭ на "Все Включено"
                                    по акционным ценам! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item cruise-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/anchor.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/anchor.png" alt="">
                                        </div>
                                        <p>Тур</p>
                                    </div>
                                </div>
                                <div class="add-fav">
                                    <i class="far fa-heart "></i>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button>Посмотреть тур!</button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="demo" class="collapse col-sm-12">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="catalog-item promo-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/percent.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img1.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/percent.png" alt="">
                                                </div>
                                                <p>Акции</p>
                                            </div>
                                        </div>
                                        <div class="add-fav">
                                            <i class="far fa-heart "></i>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button>Посмотреть тур!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="red-bg">Акции</span> Отели ОАЭ на "Все Включено"
                                            по акционным ценам! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item exclusive-tour-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/cam-ico.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img4.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/cam-ico.png" alt="">
                                                </div>
                                                <p>Экскурсионный тур</p>
                                            </div>
                                        </div>
                                        <div class="add-fav">
                                            <i class="far fa-heart "></i>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button>Посмотреть тур!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="brown-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>Мальдивы</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item tour-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/plane.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/plane.png" alt="">
                                                </div>
                                                <p>Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-fav">
                                            <i class="far fa-heart "></i>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button>Посмотреть тур!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item cruise-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/anchor.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/anchor.png" alt="">
                                                </div>
                                                <p>Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-fav">
                                            <i class="far fa-heart "></i>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button>Посмотреть тур!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 ">
                            <div class="catalog-item review-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/star-ico.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <img src="images/img2.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/star-ico.png" alt="">
                                                </div>
                                                <p>Отзыв</p>
                                            </div>
                                        </div>
                                        <div class="add-fav">
                                            <i class="far fa-heart "></i>
                                        </div>
                                        <div class="rating">
                                            <p>Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button>Посмотреть тур!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="purple-bg">Отзыв</span> Отзыв о Бодруме!</p>
                                    </div>
                                    <div class="country">
                                        <p>Греция</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item tour-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/plane.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/plane.png" alt="">
                                                </div>
                                                <p>Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-fav">
                                            <i class="far fa-heart "></i>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button>Посмотреть тур!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item review-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/star-ico.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <img src="images/img2.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/star-ico.png" alt="">
                                                </div>
                                                <p>Отзыв</p>
                                            </div>
                                        </div>
                                        <div class="add-fav">
                                            <i class="far fa-heart "></i>
                                        </div>
                                        <div class="rating">
                                            <p>Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button>Посмотреть тур!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="purple-bg">Отзыв</span> Отзыв о Бодруме!</p>
                                    </div>
                                    <div class="country">
                                        <p>Греция</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item hot-tour-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/fire.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/fire.png" alt="">
                                                </div>
                                                <p>Горящий  Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-fav">
                                            <i class="far fa-heart "></i>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button>Посмотреть тур!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="orange-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item made-kz-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/kz.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/kz.png" alt="">
                                                </div>
                                                <p>Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-fav">
                                            <i class="far fa-heart "></i>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button>Посмотреть тур!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="catalog-item exclusive-tour-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/cam-ico.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img3.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/cam-ico.png" alt="">
                                                </div>
                                                <p>Экскурсионный тур</p>
                                            </div>
                                        </div>
                                        <div class="add-fav">
                                            <i class="far fa-heart "></i>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button>Посмотреть тур!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="brown-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item hot-tour-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/fire.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/fire.png" alt="">
                                                </div>
                                                <p>Горящий  Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-fav">
                                            <i class="far fa-heart "></i>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button>Посмотреть тур!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="orange-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item promo-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/percent.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img1.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/percent.png" alt="">
                                                </div>
                                                <p>Акции</p>
                                            </div>
                                        </div>
                                        <div class="add-fav">
                                            <i class="far fa-heart "></i>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button>Посмотреть тур!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="red-bg">Акции</span> Отели ОАЭ на "Все Включено"
                                            по акционным ценам! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item cruise-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/anchor.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/anchor.png" alt="">
                                                </div>
                                                <p>Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-fav">
                                            <i class="far fa-heart "></i>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button>Посмотреть тур!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				
                <div class="col-sm-12">
                    <div class="show-more">
                        <button data-toggle="collapse" data-target="#demo">Подробнее</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="footer-list">
                    <ul>
                        <li><a href="">Компания</a></li>
                        <li><a href="">О компании</a></li>
                        <li><a href="">Новости</a></li>
                        <li><a href="">Вакансии</a></li>
                        <li><a href="">Публичная оферта</a></li>
                        <li><a href="">Партнеры</a></li>
                        <li><a href="">Акции</a></li>
                        <li><a href="">Интересное</a></li>
                        <li><a href="">Отзывы</a></li>
                        <li><a href="">Поиск туров</a></li>
                        <li><a href="">Контакты</a></li>
                    </ul>
                </div>
            </div>
            <div class="col">
                <div class="footer-list">
                    <ul>
                        <li><a href="">Каталог туров</a></li>
                        <li><a href="">Туры в рассрочку и кредит</a></li>
                        <li><a href="">Туры по Казахстану</a></li>
                        <li><a href="">Раннее бронирование</a></li>
                        <li><a href="">Экскурсионные туры</a></li>
                        <li><a href="">Туры на море</a></li>
                        <li><a href="">Оздоровительные туры</a></li>
                        <li><a href="">Купить авиабилеты</a></li>
                        <li><a href="">Туры из Алматы</a></li>
                    </ul>
                </div>
            </div>
            <div class="col">
                <div class="footer-list">
                    <ul>
                        <li><a href="">Горящие туры</a></li>
                        <li><a href="">Горящие туры в Египет</a></li>
                        <li><a href="">Горящие туры в ОАЭ</a></li>
                        <li><a href="">Горящие туры в Тайланд</a></li>
                        <li><a href="">Горящие туры в Турцию</a></li>
                        <li><a href="">Горящие туры в Грецию</a></li>
                        <li><a href="">Горящие туры на Хайнань</a></li>
                        <li><a href="">Горящие туры в Доминикану</a></li>
                        <li><a href="">Горящие туры в Чехию</a></li>
                        <li><a href="">Горящие туры в Черногорию</a></li>
                        <li><a href="">Горящие туры в Грузию</a></li>
                    </ul>
                </div>
            </div>
            <div class="col">
                <div class="footer-head">
                    <h6>Будьте всегда в курсе!</h6>
                </div>
                <div class="email-input">
                    <input type="text" placeholder="Ваша почта">
                    <button><img src="images/arrow-btn.png" alt=""></button>
                </div>
                <div class="footer-head">
                    <h6>Мы в сетях</h6>
                </div>
                <div class="socials">
                    <ul>
                        <li><a href=""><i class="fab fa-google-plus-g"></i></a></li>
                        <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href=""><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col">
                <div class="footer-head">
                    <h6>Наши контакты</h6>
                </div>
                <div class="footer-text">
                    <p><img src="images/fot-ico1.png" alt=""><a href="tel:  +7 747 094-23-00">  +7 747 094-23-00</a></p>
                    <p><img src="images/fot-ico2.png" alt=""><a href="mailto: Travelmarket.info"> Travelmarket.info</a></p>
                    <p><img src="images/fot-ico3.png" alt="">Алматы, Назарбаева 132,
                        6 этаж, офис 6</p>
                </div>
            </div>
        </div>
    </div>
    <div class="border-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="footer-text">
                        <p>2014-2019 © Travelmarket.kz  </p>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="footer-text text-center">
                        <p>Сайт разработан в: <a href="https://www.a-lux.kz/">A-Lux </a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

</body>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" ></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/main.js"></script>


</html>

