(function($) {
    $('.img').click(function () {
        $('.search-area input').show();
    });
    $('.add-fav').hover(function () {
        $('.add-fav').toggleClass('wow animated heartBeat');
    });
    $('.close-menu').click(function (e) {
        e.preventDefault();
        $('.menu').hide();
    });
    $('.toggle-menu').click(function (e) {
        e.preventDefault();
        $('.menu').slideDown();
    });
  
    $('.show-all').click(function () {
        $('.tour-item,.hot-tour-item, .cruise-item, .made-kz-item, .promo-item, .review-item, .exclusive-tour-item').addClass('wow animated fadeIn').show();
    });
    $('.tour').click(function () {
        $('.tour-item').addClass('wow animated fadeIn').show();
        $('.hot-tour-item, .cruise-item, .made-kz-item, .promo-item, .review-item, .exclusive-tour-item').hide();
    });
    $('.hot-tour').click(function () {
        $('.hot-tour-item').addClass('wow animated fadeIn').show();
        $('.tour-item, .cruise-item, .made-kz-item, .promo-item, .review-item, .exclusive-tour-item').hide();
    });
    $('.cruise').click(function () {
        $('.cruise-item').addClass('wow animated fadeIn').show();
        $('.tour-item, .hot-tour-item, .made-kz-item, .promo-item, .review-item, .exclusive-tour-item').hide();
    });
    $('.made-kz').click(function () {
        $('.made-kz-item').addClass('wow animated fadeIn').show();
        $('.tour-item, .hot-tour-item, .cruise-item, .promo-item, .review-item, .exclusive-tour-item').hide();
    });
    $('.promo').click(function () {
        $('.promo-item').addClass('wow animated fadeIn').show();
        $('.tour-item, .hot-tour-item, .cruise-item, .made-kz-item, .review-item, .exclusive-tour-item').hide();
    });
    $('.reviews').click(function () {
        $('.review-item').addClass('wow animated fadeIn').show();
        $('.tour-item, .hot-tour-item, .cruise-item, .made-kz-item, .promo-item, .exclusive-tour-item').hide();
    });
    $('.exclusive-tour').click(function () {
        $('.exclusive-tour-item').addClass('wow animated fadeIn').show();
        $('.tour-item, .hot-tour-item, .cruise-item, .made-kz-item, .promo-item, .review-item').hide();
    });
    new WOW().init();
})(jQuery);
$(document).mouseup(function(e)
{
    var container = $(".search-area input");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        container.hide(0);
    }
});



