<?php include "layout/header.php"; ?>
    <!-- sales -->
    <section class="sales faq-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="breadcrums">
                        <li>
                            <a href="index.php">Главная страница</a>
                        </li>
                        <li class="brd-line">
                            <a href="#">-</a>
                        </li>
                        <li>
                            <a href="akcii.php">Акции и скидки</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 ">
                    <h1 class="sales-title">Акции и скидки</h1>
                </div>
            </div>
            <div class="row">
                <!-- card sales-->
                <div class="card col-mb-12 col-lg-12 col-sm-12">
                    <div class="row no-gutters">
                        <div class="col-md-4 col-lg-4 col-sm-12 card-image">
                            <img src="images/sales-img1.png" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <div class="card-body">
                                <p class="sales-day-over">Акция закончилась</p>
                                <h5 class="card-title"> <a href="card-tour.php" target="_blank">Супер Акция в самый популярный семейный отель Греции - Porto
                                    Carras Sithonia 5*</a></h5>
                                <p class="card-text">Бесплатный Upgrade номера в Porto Carras Sithonia 5* до 30 июня!
                                    +Дети до 14 лет отдыхают БЕСПЛАТНО! Гарантия лучшей цены до 30 июня,
                                    До 30 июня 2019 года отель Porto Carras Sithonia Hotel 5* предоставляет номера
                                    категории Double Superior Room Side Sea View по стоимости Double Superior Room Golf
                                    View!
                                    Вид на море без всяких доплат!</p>
                                <ul>
                                    <li>
                                        Бронирование — с 14.06.2019 по 30.06.2019;
                                    </li>
                                    <li>
                                        Проживание — 11.07.2019 по 29.10.2019;
                                    </li>
                                    <li>
                                        При бронировании нужно выбрать Double Superior Golf View/Side Sea View.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card sales -->
                <!-- card sales-->
                <div class="card col-mb-12 col-lg-12 col-sm-12">
                    <div class="row no-gutters">
                        <div class="col-md-4 col-lg-4 col-sm-12 card-image">
                            <img src="images/sales-img2.png" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <div class="card-body">
                                <p class="sales-day-left">Осталось 2 дня</p>
                                <h5 class="card-title"><a href="card-tour.php" target="_blank"> Египет из Алматы. Горящие туры на вылет с 29 июня по 29 июля</a></h5>
                                <p class="card-text">Отдых на Красном море по выгодным ценам! Отдыхайте всей семьей на
                                    побережье Красного моря. Египет - это удивительная страна, где каждый сможет найти
                                    для себя занятие по вкусу, шикарное море бирюзового цвета, яркий и богатый подводный
                                    мир, самый лучший дайвинг и снорклинг в мире, а также загадочные пирамиды не оставят
                                    вас равнодушными. Отели в Египте работают по системе питания - All Inclusive.
                                    Подарите незабываемый отдых себе и своим детям на берегу Красного моря.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card sales -->
                <!-- card sales-->
                <div class="card col-mb-12 col-lg-12 col-sm-12">
                        <div class="row no-gutters">
                            <div class="col-md-4 col-lg-4 col-sm-12 card-image">
                                <img src="images/sales-img3.png" class="card-img" alt="...">
                            </div>
                            <div class="col-md-8 col-lg-8 col-sm-12">
                                <div class="card-body">
                                    <p class="sales-day-valid">Акция действует</p>
                                    <h5 class="card-title"><a href="card-tour.php" target="_blank"> ОАЭ из Алматы. Горящие туры на вылет с 30 июня по 30 июля</a></h5>
                                    <p class="card-text">Бесплатный Upgrade номера в Porto Carras Sithonia 5* до 30 июня!
                                        +Дети до 14 лет отдыхают БЕСПЛАТНО! Гарантия лучшей цены до 30 июня,
                                        До 30 июня 2019 года отель Porto Carras Sithonia Hotel 5* предоставляет номера
                                        категории Double Superior Room Side Sea View по стоимости Double Superior Room Golf
                                        View!
                                        Вид на море без всяких доплат!</p>
                                    <ul>
                                        <li>
                                            Бронирование — с 14.06.2019 по 30.06.2019;
                                        </li>
                                        <li>
                                            Проживание — 11.07.2019 по 29.10.2019;
                                        </li>
                                        <li>
                                            При бронировании нужно выбрать Double Superior Golf View/Side Sea View.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end card sales -->
                    <!-- card sales-->
                <div class="card col-mb-12 col-lg-12 col-sm-12">
                    <div class="row no-gutters">
                        <div class="col-md-4 col-lg-4 col-sm-12 card-image">
                            <img src="images/sales-img4.png" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <div class="card-body">
                                <p class="sales-day-over">Акция закончилась</p>
                                <h5 class="card-title"><a href="card-tour.php" target="_blank">Китай (о.Хайнань) из Алматы. Горящие туры на вылет с 2 июля</a> </h5>
                                <p class="card-text">Внимание! Гарантированные места на рейс из Алматы в Санью, прямым рейсом! А также есть прямые рейсы до аэропорта Хайкоу. Еще более низкие цены только сейчас! Спешите скорее забронировать тур на Тропический остров Хайнань. Вылеты каждую субботу и пятницу на 8 и 15 дней.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card sales -->
            </div>
        </div>
    </section>
    <!-- end sales -->
    <?php include "layout/footer.php"; ?>
