<!--    ================================================== -->

</div>
<a href="#" id="back-to-top" title="Back to top"><i class="fa fa-arrow-up"></i> </a>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="footer-list">
                    <ul>
                      <li><a href="akcii.php">Акции</a></li>
                        <li><a href="company.php">Компания</a></li>
                        <li><a href="faq.php">Вопросы  и ответы</a></li>
                        <li><a href="contacts.php">Контакты</a></li>
                        <li><a href="sravnenie.php">Сравнение</a></li>
                    </ul>
                </div>
            </div>
            <!-- <div class="col">
                <div class="footer-list">
                    <ul>
                        <li><a href="">Туры в рассрочку и кредит</a></li>
                        <li><a href="">Туры по Казахстану</a></li>
                        <li><a href="">Раннее бронирование</a></li>
                        <li><a href="">Экскурсионные туры</a></li>
                        <li><a href="">Туры на море</a></li>
                        <li><a href="">Оздоровительные туры</a></li>
                        <li><a href="">Купить авиабилеты</a></li>
                        <li><a href="">Туры из Алматы</a></li>
                    </ul>
                </div>
            </div>
            <div class="col">
                <div class="footer-list">
                    <ul>
                        <li><a href="card-tour.php">Горящие туры</a></li>
                        <li><a href="card-tour.php">Горящие туры в Египет</a></li>
                        <li><a href="card-tour.php">Горящие туры в ОАЭ</a></li>
                        <li><a href="card-tour.php">Горящие туры в Тайланд</a></li>
                        <li><a href="card-tour.php">Горящие туры в Турцию</a></li>
                        <li><a href="card-tour.php">Горящие туры в Грецию</a></li>
                        <li><a href="card-tour.php">Горящие туры на Хайнань</a></li>
                        <li><a href="card-tour.php">Горящие туры в Доминикану</a></li>
                        <li><a href="card-tour.php">Горящие туры в Чехию</a></li>
                        <li><a href="card-tour.php">Горящие туры в Черногорию</a></li>
                        <li><a href="card-tour.php">Горящие туры в Грузию</a></li>
                    </ul>
                </div>
            </div> -->
            <div class="col">
                <div class="footer-head">
                    <h6>Будьте всегда в курсе!</h6>
                </div>
                <div class="email-input">
                    <input type="text" placeholder="Ваша почта">
                    <button><img src="images/arrow-btn.png" alt=""></button>
                </div>
                <div class="footer-head">
                    <h6>Мы в сетях</h6>
                </div>
                <div class="socials">
                    <ul>
                        <li><a href=""><i class="fab fa-google-plus-g"></i></a></li>
                        <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href=""><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col">
                <div class="footer-head">
                    <h6>Наши контакты</h6>
                </div>
                <div class="footer-text">
                    <p><img src="images/fot-ico1.png" alt=""><a href="tel:  +7 747 094-23-00">  +7 747 094-23-00</a></p>
                    <p><img src="images/fot-ico2.png" alt=""><a href="mailto: Travelmarket.info"> Travelmarket.info</a></p>
                    <p><img src="images/fot-ico3.png" alt="">Алматы, Назарбаева 132,
                        6 этаж, офис 6</p>
                </div>
            </div>
        </div>
    </div>
    <div class="border-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="footer-text">
                        <p>2014-2019 © Travelmarket.kz  </p>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="footer-text text-center">
                        <p>Сайт разработан в: <a href="https://www.a-lux.kz/">A-Lux </a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

</body>
<script type="text/javascript" src="js/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" ></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js" integrity="sha256-arMsf+3JJK2LoTGqxfnuJPFTU4hAK57MtIPdFpiHXOU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" integrity="sha256-Uv9BNBucvCPipKQ2NS9wYpJmi8DTOEfTA/nH2aoJALw=" crossorigin="anonymous"></script>

<script src="js/main.js"></script>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.4/cropper.min.js"></script>

</html>
