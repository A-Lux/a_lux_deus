  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Travelmarket</title>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" href="css/owl.carousel.min.css">
      <link rel="stylesheet" href="css/owl.theme.default.min.css">
      <link rel="stylesheet" href="css/animate.css">
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css" integrity="sha256-IvM9nJf/b5l2RoebiFno92E5ONttVyaEEsdemDC6iQA=" crossorigin="anonymous" />
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" >
      <!-- CSS Plugins
    ================================================== -->
      <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.4/cropper.min.css">
      <link rel="stylesheet" href="css/slick.css">
      <link rel="stylesheet" href="css/slick-theme.css">

      <!-- Favicons
    ================================================== -->
      <link rel="shortcut icon" href="#">
      <!-- CSS FontAwesome
    ================================================== -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
          <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
          <script src="https://api-maps.yandex.ru/2.1/?apikey=<14310a90-12c2-43f9-ad71-aa9c0b766129>&lang=ru_RU" type="text/javascript"></script>
	  <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

  </head>
  <body>
  <div class="content">
      <header class="header">
          <div class="head-menu">
              <div class="container">
                  <div class="row modile-rowss">
                      <div class="col-sm-2">
                          <div class="logo">
                              <a href="index.php">
                                  <img src="images/logo.png" alt="">
                              </a>
                          </div>
                      </div>
                      <div class="col-sm-2">
                          <div class="phone">
                              <a href="tel:87273337060"><img src="images/phone-ico.png" alt="">3337060</a>
                          </div>
                      </div>
                     <div class="offset-lg-4 col-sm-2 desktop-cabinet">
                         <div class="cabinet">
                             <a href="lk-dews.php"><img src="images/4229498-32.png" width="24px">Кабинет туриста <img src="images/drop-down-arrow.png" width="12px"></a>
                         </div>
                     </div>
                      <div class="col-sm-2 desktop-cabinet">
                          <div class="toggle-menu">
                              <a href="">Меню <img src="images/menu.png" alt=""></a>
                          </div>
                      </div>

  					<div class="mobile-cabinet">
  						<div class="row">
  						<div class="cabinet">
  							<a href=""><img src="images/4229498-32.png" width="24px"></a>
  						</div>


  							<div class="toggle-menu">
  								<a href=""><img src="images/menu.png" alt=""></a>
  							</div>
  						</div>
  					</div>
  				</div>
              </div>
          </div>
         <div class="container">
             <div class="menu" id="id01">
                 <div class="user-row">
                     <!-- <div class="name">
                         <a href=""><p><img src="images/arrow.png" alt=""></p></a>
                     </div> -->
                     <!-- <div class="avatar">
                         <a href=""><img src="images/avatar.png" alt=""></a>
                     </div> -->
                     <div class="close-menu">
                         <a href=""><img src="images/menu-arrow.png" alt=""></a>
                     </div>
                 </div>
                 <ul>
                     <li><p><a href="akcii.php">Акции</a></p></li>
                     <li><p><a href="company.php">О компании</a></p></li>
                     <li><p><a href="faq.php">Вопросы и ответы</a></p></li>
                     <li><p><a href="sravnenie.php">Сравнение</a></p></li>
                     <li class="space-top"><a href=""><div class="icon"><img src="images/menu-icon1.png" alt=""></div>+7 771 756 65 88</a></li>
                     <li><a href=""><div class="icon"><img src="images/menu-icon2.png" alt=""></div>@travel.info</a></li>
                 </ul>
             </div>
             <div class="main-title">
                 <h1>Выбирайте и покупайте туры онлайн</h1>
                 <p>по ценам туроператоров или ниже с круглосуточной <br>
                     поддержкой до и после покупки</p>
             </div>
             <div class="pick-tour">
                 <button>Подберите мне тур</button>
             </div>
             <ul class="nav nav-tabs" id="myTab" role="tablist">
                 <li class="nav-item">
                     <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-plane"></i>ТУР</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fas fa-fire-alt"></i>ГОРЯЩИЙ ТУР</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"><i class="fas fa-hotel"></i>ОТЕЛИ</a>
                 </li>
             </ul>
             <div class="tab-content" id="myTabContent">
                 <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                     <div class="input-row">
                         <div class="input-row-item1">
                             <input type="text" placeholder="Откуда">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item2">
                             <input type="text" placeholder="Куда">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item3">
                             <input type="text" placeholder="Когда">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item4">
                             <input type="text" placeholder="Длительность">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item5">
                             <input type="text" placeholder="Туристы">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item6">
                             <input type="text" placeholder="Кол-во звезд">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="search-area">
  <!--                           <input type="text" placeholder="Поиск">-->
                             <div class="img">
                                 <img src="images/search.png" alt="">
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                     <div class="input-row">
                         <div class="input-row-item1">
                             <input type="text" placeholder="Откуда">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item2">
                             <input type="text" placeholder="Куда">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item3">
                             <input type="text" placeholder="Когда">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item4">
                             <input type="text" placeholder="Длительность">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item5">
                             <input type="text" placeholder="Туристы">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item6">
                             <input type="text" placeholder="Кол-во звезд">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="search-area">
                            <!-- <input type="text" placeholder="Поиск"> -->
                             <div class="img">
                                 <img src="images/search.png" alt="">
                             </div>
                         </div>
                     </div>
                 </div>

                 <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                     <div class="input-row">
                         <div class="input-row-item1">
                             <input type="text" placeholder="Откуда">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item2">
                             <input type="text" placeholder="Куда">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item3">
                             <input type="text" placeholder="Когда">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item4">
                             <input type="text" placeholder="Длительность">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item5">
                             <input type="text" placeholder="Туристы">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="input-row-item6">
                             <input type="text" placeholder="Кол-во звезд">
  						   <img src="images/drop-down-arrow.png" width="12px">
                         </div>
                         <div class="search-area">
                            <!-- <input type="text" placeholder="Поиск"> -->
                             <div class="img">
                                 <img src="images/search.png" alt="">
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
      </header>
