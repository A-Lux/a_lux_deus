<?php include "layout/header.php"; ?>

 <!-- sales -->
        <div class="container" id="card-tour">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="breadcrums">
                        <li>
                            <a href="index.php">Главная</a>
                        </li>
                        <li>
                            <a href="#"> / </a>
                        </li>
                         <li>
                            <a href="card-tour.php"> Туры</a>
                        </li>
                         <li>
                            <a href="#"> / </a>
                        </li>
                         <li>
                            <a href="#"> Название туры</a>
                        </li>
                    </ul>
                </div>
    </div>


            <div class="tour-block">
              <a href="#">Тур</a>
              <a href="#">Горящий тур</a>


              <div class="tour-block-title">
                <h4>Сказочные Мальдивы на "Все Включено</h4>
                <p>Мальдивы</p>
              </div>
            </div>

            <div class="col-sm-12">
            <div class="wrapper">
              <div class="slick-Ban">
                <div class="slick-item"><img src="images/slider-tour-1.png"></div>
                <div class="slick-item"><img src="images/slider-tour-1.png"></div>
                <div class="slick-item"><img src="images/slider-tour-1.png"></div>
                <div class="slick-item"><img src="images/slider-tour-1.png"></div>
                <div class="slick-item"><img src="images/slider-tour-1.png"></div>
                <div class="slick-item"><img src="images/slider-tour-1.png"></div>
                <div class="slick-item"><img src="images/slider-tour-1.png"></div>
                <div class="slick-item"><img src="images/slider-tour-1.png"></div>
              </div>

              <div class="slick-Nav">
                <div class="slick-item small-slick-img"><img src="images/small-slider-1.png" alt=""></div>
                <div class="slick-item small-slick-img"><img src="images/small-slider-2.png" alt=""></div>
                <div class="slick-item small-slick-img"><img src="images/small-slider-3.png" alt=""></div>
                <div class="slick-item small-slick-img"><img src="images/small-slider-4.png" alt=""></div>
                <div class="slick-item small-slick-img"><img src="images/small-slider-5.png" alt=""></div>
                <div class="slick-item small-slick-img"><img src="images/small-slider-6.png" alt=""></div>
                <div class="slick-item small-slick-img"><img src="images/small-slider-6.png" alt=""></div>
                <div class="slick-item small-slick-img"><img src="images/small-slider-6.png" alt=""></div>
                
              </div>
            </div>
            <div class="check-tabs nav-tabs">
              <a href="#reserve" class="btn-checked check-link">Бронирование</a>
              <a href="#desc" class="check-link">Описание</a>
              <a href="#reviews" class="check-link">Отзывы</a>
              <a href="#places" class="check-link">Расположение</a>
            </div>


            <div class="filter-tours " >
              <h4>Подобрать варианты в «Сказочные Мальдивы»</h4>
              <div class="filter-items d-flex col-sm-12">
                <div class="item-title col-sm-2">Сказочные Мальдивы на "Все Включено"!</div>
                <div class="item-fromcity col-sm-2">
                  <p>Город вылета</p>
                  <select class="" name="">
                    <option value="almaty">Алматы</option>
                    <option value="almaty">Нур-Султан</option>
                  </select>
                </div>
                <div class="item-fordays col-sm-2">
                  <p>Кол-во дней</p>
                  <select class="" name="">
                    <option value="7">7 дней</option>
                    <option value="4">4 дней</option>
                  </select>
                </div>
                <div class="item-nutrition col-sm-2">
                  <p>Питание</p>
                  <select class="" name="">
                    <option value="all">Все включено</option>
                    <option value="4">4 дней</option>
                  </select>
                </div>
                <div class="item-date col-sm-2">
                  <p>Питание</p>
                  <select class="" name="">
                    <option value="all">Все включено</option>
                    <option value="4">4 дней</option>
                  </select></div>
                <div class="item-people col-sm-2">
                  <p>Кол-во людей</p>
                  <select class="" name="">
                    <option value="all">2 взрослых</option>
                    <option value="4">4 дней</option>
                  </select></div>
              </div>
              <div class="edit-filter">
                <button type="button" name="button"> <img src="images/edit.png" alt=""> Изменить</button>
              </div>
            </div>
            <div class="tour-dates">
              <h4>Найдите выгодные даты тура</h4>


<div style="width: 85%" id="bardiv">

  <div style="width: 1524px;" >
      <canvas id="barcanvas"></canvas>
  </div>
</div>
<div class="btns">
  <button onclick="scrollWin2()" id="toleft" name="button"><i class="fa fa-angle-left"></i> </button>
  <button onclick="scrollWin()" id="toright" name="button"> <i class="fa fa-angle-right"></i></button>
</div>


            </div>
            <div class="tour-table col-sm-12">
              <ul class="titlestable">
                <li>Город вылета</li>
                <li>Продолжительность</li>
                <li>Питание</li>
                <li>Тип номера</li>
                <li>Кол-во людей</li>
                <li>Цена за тур</li>
              </ul>
              <ul class="contenttable active">
                <li>Алматы</li>
                <li>20.07-27.07  <strong>7 дней</strong></li>
                <li>Все включено</li>
                <li>Double suite</li>
                <li>2 взрослых</li>
                <li class="table-price">400 000 kzt</li>
              </ul>
              <ul class="contenttable">
                <li>Алматы</li>
                <li>20.07-27.07  <strong>7 дней</strong></li>
                <li>Все включено</li>
                <li>Double suite</li>
                <li>2 взрослых</li>
                <li class="table-price">400 000 kzt</li>
              </ul>
              <ul class="contenttable">
                <li>Алматы</li>
                <li>20.07-27.07  <strong>7 дней</strong></li>
                <li>Все включено</li>
                <li>Double suite</li>
                <li>2 взрослых</li>
                <li class="table-price">400 000 kzt</li>
              </ul>
              <ul class="contenttable">
                <li>Алматы</li>
                <li>20.07-27.07  <strong>7 дней</strong></li>
                <li>Все включено</li>
                <li>Double suite</li>
                <li>2 взрослых</li>
                <li class="table-price">400 000 kzt</li>
              </ul>

              <nav aria-label="Page navigation example">
    <ul class="pagination">
      <li class="page-item"><a class="page-link" href="#">В начало</a></li>
      <li class="page-item"><a class="page-link" href="#">1</a></li>
      <li class="page-item"><a class="page-link" href="#">2</a></li>
      <li class="page-item"><a class="page-link" href="#">3</a></li>
      <li class="page-item"><a class="page-link" href="#">в конец</a></li>
    </ul>
  </nav>
  <div class="social-blocks col-sm-12 d-flex">
    <p>Поделиться этим туром:</p>
  <span><a href="#"><i class="fa fa-instagram"></i></a></span>
  <span><a href="#"><i class="fa fa-vk"></i></a></span>
  <span><a href="#"><i class="fa fa-facebook"></i></a></span>
  </div>
</div>
<div class="mobile-tourtable-block">
  <ul class="mobile-tourtable">
    <li><strong>Город вылета:<br> </strong> Алматы </li>
    <li><strong>Продолжительность:<br> </strong> 20.07-27.07 7 дней </li>
    <li><strong>Питание:<br> </strong> Все включено</li>
    <li><strong>Тип номера:<br> </strong>Double suite </li>
    <li><strong>Кол-во людей:<br> </strong> 2 взрослых</li>
    <li><strong>Цена за тур: <br> </strong> 400 000 kzt</li>
  </ul>
</div>
            <div class="recommended" id="recommended">
              <h5 class="recom-title">Рекомендуем</h5>
              <div class="catalog-tour d-flex">
                    <div class="catalog-item promo-item col-sm-4">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/percent.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img1.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/percent.png" alt="">
                                        </div>
                                        <p>Акции</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="red-bg">Акции</span> Отели ОАЭ на "Все Включено"
                                    по акционным ценам! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item exclusive-tour-item col-sm-4">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/cam-ico.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img4.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/cam-ico.png" alt="">
                                        </div>
                                        <p>Экскурсионный тур</p>
                                    </div>
                                </div>

                                <div class="add-comp">
                                <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="brown-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>Мальдивы</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item tour-item col-sm-4">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/plane.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/plane.png" alt="">
                                        </div>
                                        <p>Тур</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
            <div class="saw-tours" id="saws">
              <h5 class="recom-title">Ранее вы смотрели</h5>
              <div class="latest-seen slider">
                <div class=" slick-item small-card col-sm-4">
                  <a href="#">
                  <div class="sm-image col-sm-5">
                    <img src="images/img5.jpg" alt="" width="100%">
                  </div>
                  <div class="sm-content col-sm-7">
                    <h6>ТОП 10 попуярных отелей Греции! Перелет из Алматы</h6>
                    <p>Греция</p>
                  </div>
                  </a>
                </div>
                <div class=" slick-item small-card col-sm-4">
                  <a href="#">
                  <div class="sm-image col-sm-5">
                    <img src="images/img5.jpg" alt="" width="100%">
                  </div>
                  <div class="sm-content col-sm-7">
                    <h6>ТОП 10 попуярных отелей Греции! Перелет из Алматы</h6>
                    <p>Греция</p>
                  </div>
                  </a>
                </div>
                <div class=" slick-item small-card col-sm-4">
                  <a href="#">
                  <div class="sm-image col-sm-5">
                    <img src="images/img5.jpg" alt="" width="100%">
                  </div>
                  <div class="sm-content col-sm-7">
                    <h6>ТОП 10 попуярных отелей Греции! Перелет из Алматы</h6>
                    <p>Греция</p>
                  </div>
                  </a>
                </div>
                <div class=" slick-item small-card col-sm-4">
                  <a href="#">
                  <div class="sm-image col-sm-5">
                    <img src="images/img5.jpg" alt="" width="100%">
                  </div>
                  <div class="sm-content col-sm-7">
                    <h6>ТОП 10 попуярных отелей Греции! Перелет из Алматы</h6>
                    <p>Греция</p>
                  </div>
                  </a>
                </div>
                <div class=" slick-item small-card col-sm-4">
                  <a href="#">
                  <div class="sm-image col-sm-5">
                    <img src="images/img5.jpg" alt="" width="100%">
                  </div>
                  <div class="sm-content col-sm-7">
                    <h6>ТОП 10 попуярных отелей Греции! Перелет из Алматы</h6>
                    <p>Греция</p>
                  </div>
                  </a>
                </div>
              </div>
            </div>



</div>
<?php include "layout/footer.php"; ?>
