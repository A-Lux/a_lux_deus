<?php include "layout/header.php"; ?>

 <!-- sales -->
    <section class="lk-dews-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="breadcrums">
                        <li>
                            <a href="index.php">Главная страница</a>
                        </li>
                        <li>
                            <a href="#"> / </a>
                        </li>
                        <li>
                            <a href="#">Личный кабинет</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="personal-area-title">
              <h2>Личный кабинет</h2>
            </div>
            <div class="personal-area-block">
              <div class="row">
                <div class="col-12 col-lg-4">
                  <div class="user">
                    <img src="images/personal-user.png" alt="">
                    <div>
                      <p>Евгений</p>
                      <a href="#">yevgen86@gmail.com</a>
                    </div>
                  </div>
                  <div class="user-tab">
                    <ul>
                      <li data-tab="tab-1" class="user-tab-active"><i class="far fa-user"></i> Личные данные</li>
                      <li data-tab="tab-2" ><i class="far fa-heart"></i> Избранное</li>
                      <li data-tab="tab-3" class="voucher-active"><img src="images/voucher.png">Промо код</li>
                      <li data-tab="tab-4" ><i class="fas fa-history"></i> История заказов</li>
                    </ul>
                  </div>
                </div>

                <div class="col-12 col-lg-8">
                 <div class="user-tab-1 user-block-active" id="tab-1">
                   <h4>Историй заказов</h4>
                   <table class="table table-responsive">
                    <tbody>
                      <tr>
                        <td scope="row">Номер заказа</td>
                        <td>Наименование турa</td>
                        <td>Дата заказа</td>
                        <td>Стоимость</td>
                        <td>Статус</td>
						 <td></td>

                      </tr>
                       <tr>
                        <td scope="row">23364593</td>
                        <td class="text-primary"><b>Отели ОАЭ на "Все <br>Включено" по акционным <br>ценам! Вылет из <br>Алматы! <br><span class="text-muted">ОАЭ</span></b></td>
                        <td>22. 06. 2019</td>
                        <td class="text-primary"><b>230 000 KZT</b></td>
                        <td>Оплачено</td>
						   <td><span class="read-circles">&#9679;&#9679;&#9679;</span></td>
                      </tr>
                       <tr>
                        <td scope="row">23364593</td>
                        <td class="text-primary"><b>Отели ОАЭ на "Все <br>Включено" по акционным <br>ценам! Вылет из <br>Алматы! <br><span class="text-muted">ОАЭ</span></b></td>
                        <td>22. 06. 2019</td>
                        <td class="text-primary"><b>230 000 KZT</b></td>
                        <td>Оплачено</td>
						   <td><span class="read-circles">&#9679;&#9679;&#9679;</span></td>

                      </tr>
                      <tr>
                        <td scope="row">23364593</td>
                        <td class="text-primary"><b>Отели ОАЭ на "Все <br>Включено" по акционным <br>ценам! Вылет из <br>Алматы! <br><span class="text-muted">ОАЭ</span></b></td>
                        <td>22. 06. 2019</td>
                        <td class="text-primary"><b>230 000 KZT</b></td>
                        <td>Оплачено</td>
						  <td><span class="read-circles">&#9679;&#9679;&#9679;</span></td>

                      </tr>
                      <tr>
                        <td scope="row">23364593</td>
                        <td class="text-primary"><b>Отели ОАЭ на "Все <br>Включено" по акционным <br>ценам! Вылет из <br>Алматы! <br><span class="text-muted">ОАЭ</span></b></td>
                        <td>22. 06. 2019</td>
                        <td class="text-primary"><b>230 000 KZT</b></td>
                        <td>Оплачено</td>
						  <td><span class="read-circles">&#9679;&#9679;&#9679;</span></td>

                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="user-tab-1"  id="tab-2">
                   <h4>Избранное</h4>
                   <div class="col-sm-12">


                   <div class="сol-sm-6">
                   <div class="catalog-item review-item">
                       <div class="image">
                           <div class="filter-wrap">
                               <div class="filter-item">
                                   <div class="image">
                                       <img src="images/star-ico.png" alt="">
                                   </div>
                               </div>
                           </div>
                           <img src="images/img2.jpg" class="img-fluid" alt="">
                           <div class="hover-overlay">
                               <div class="filter-wrap">
                                   <div class="filter-item">
                                       <div class="image">
                                           <img src="images/star-ico.png" alt="">
                                       </div>
                                       <p>Отзыв</p>
                                   </div>
                               </div>
                               <div class="add-comp">
                                 <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                               </div>
                               <div class="add-fav">
                                   <i class="far fa-heart "></i>
                               </div>
                               <div class="rating">
                                   <p>Luxor Resort <img src="images/rating.png" alt=""></p>
                               </div>
                               <div class="button-row">
                                   <button>Все продано</button>
                                   <button><a href="card-tour.php">Посмотреть тур!</a></button>

                               </div>
                           </div>
                       </div>
                       <div class="description">
                           <div class="promo ">
                               <p><span class="purple-bg">Отзыв</span> Отзыв о Бодруме!</p>
                           </div>
                           <div class="country">
                               <p>Греция</p>
                           </div>
                       </div>
                   </div>
                 </div>
               </div>
					</div>
                <div class="user-tab-1"  id="tab-3">
                   <h4>Историй заказов-3</h4>
                   <table class="table table-responsive">
                    <tbody>
                      <tr>
                        <td scope="row">Номер заказа</td>
                        <td>Наименование турa</td>
                        <td>Дата заказа</td>
                        <td>Стоимость</td>
                        <td>Статус</td>
                      </tr>
                      <tr>
                        <td scope="row">23364593</td>
                        <td class="text-primary">Отели ОАЭ на "Все <br>Включено" по акционным <br>ценам! Вылет из <br>Алматы! <br><span class="text-muted">ОАЭ</span></td>
                        <td>22. 06. 2019</td>
                        <td>230 000 KZT</td>
						  <td>Оплачено</td>
						  <td><span class="read-circles">&#9679;&#9679;&#9679;</span></td>
                      </tr>
                      <tr>
                        <td scope="row">23364593</td>
                        <td class="text-primary">Отели ОАЭ на "Все <br>Включено" по акционным <br>ценам! Вылет из <br>Алматы! <br><span class="text-muted">ОАЭ</span></td>
                        <td>22. 06. 2019</td>
                        <td>230 000 KZT</td>
                        <td>Оплачено</td>
						   <td><span class="read-circles">&#9679;&#9679;&#9679;</span></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="user-tab-1"  id="tab-4">
                   <h4>Историй заказов-4</h4>
                   <table class="table table-responsive">
                    <tbody>
                      <tr>
                        <td scope="row">Номер заказа</td>
                        <td>Наименование турa</td>
                        <td>Дата заказа</td>
                        <td>Стоимость</td>
                        <td>Статус</td>
                      </tr>
                      <tr>
                        <td scope="row">23364593</td>
                        <td class="text-primary">Отели ОАЭ на "Все <br>Включено" по акционным <br>ценам! Вылет из <br>Алматы! <br><span class="text-muted">ОАЭ</span></td>
                        <td>22. 06. 2019</td>
                        <td>230 000 KZT</td>
                        <td>Оплачено</td>
						  <td><span class="read-circles">&#9679;&#9679;&#9679;</span></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php include "layout/footer.php"; ?>
