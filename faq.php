<?php include "layout/header.php"; ?>


    <!-- faq -->
    <section class="faq-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="breadcrums">
                        <li>
                            <a href="index.php">Главная страница</a>
                        </li>
                        <li class="brd-line">
                            <a href="#">-</a>
                        </li>
                        <li>
                            <a href="#">О компании</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <p class="faq-title">Вопросы и ответы</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <form class="form-inline">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Умный поиск по FAQ">
                            <div class="input-group-prepend">
                                <button>
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="fast-search-title">
                        <p>Быстрый поиск</p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="fast-search-tabs">
                        <li>
                            <a class="fast-search-tab-active" href="#1">Виза</a>
                        </li>
                        <li>
                            <a href="#2">Отель</a>
                        </li>
                        <li>
                            <a href="#3">Перелет</a>
                        </li>
                        <li>
                            <a href="#4">Бронирование и оплата</a>
                        </li>
                        <li>
                            <a href="#5">Все</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <!-- Виза -->
                    <div class="faq-content faq-content-active" id="1" style="display:none;">
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Правила расчета заявок при работе по системе тарифов</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo" class="collapse collapse-text">
                            <p>Тарифы и расписание авиперелетов на рейсах иностранных авиакомпаний указаны на сайте. На главной странице в крайней левой колонке на голубом фоне перечислены все страны под своими флагами, находим страну Доминикана, под названием страны находим ссылку “Авиа перелеты”, открываем ее, находим файл для загрузки “Стоимость и расписание регулярных рейсов на Пунта Кана”,  открываем его. Наши туристы захотели перелет через Амстердам, находим нужную авиакомпанию “KLM + MARTINAIR”. В таблице “Расписание регулярных рейсов KLM (KL) + Martinair (MP) с пересадкой в Амстердаме (AMS) ” находим наш маршрут и его расписание: 11,10 в 4 день недели (четверг), Ш-2 – Амстердам и затем Амстердам-Пунта Канна.
Ниже в  таблице “Стоимость авиабилетов, включая таксы и сборы” находим стоимость авиабилетов:
Минимальный тариф на этом маршруте – 965 евро с человека. Условия - min правило воскр.= тур обязательно внутри себя должен содержать день недели воскресенье, т е если полет с понедельника по субботу – этот тариф не действует, max 1 месяц = максимальное количество пребывания в стране 1 месяц.  Оба условия у нас выполняются. Но для окончательного подтверждения тарифа необходимо сделать бронь и получить на нее подтверждение (мы все это делаем через систему бронирования авиакомпании и высылаем вам).
Цена авиабилета указана за маршрут туда - обратно и включает: место в самолете, питание, разрешение на провоз определенного количества кг багажа (зависит от класса перелета и указано в авиабилете). Цена авиабилета указано в евро, но оплата будет производиться по курсу, который указан перед списком тарифов.
                            </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo2">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Инструкция по расчету сложных туров по суточным ценам (Доминикана)</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo2" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo3">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Правила расчета заявок при работе по системе тарифов</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo3" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo4">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Скачать иллюстрированную инструкцию по работе с главной страницей сайта</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo4" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo5">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Правила предварительного бронирования</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo5" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo6">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Инструкция по расчету сложных туров по суточным ценам (Доминикана)</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo6" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>

                    </div>
                    <!-- Отель -->
                    <div class="faq-content" id="2" style="display:none;">
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo11">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Правила расчета заявок при работе по системе тарифов</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo11" class="collapse collapse-text">
                            <p>Тарифы и расписание авиперелетов на рейсах иностранных авиакомпаний указаны на сайте. На главной странице в крайней левой колонке на голубом фоне перечислены все страны под своими флагами, находим страну Доминикана, под названием страны находим ссылку “Авиа перелеты”, открываем ее, находим файл для загрузки “Стоимость и расписание регулярных рейсов на Пунта Кана”,  открываем его. Наши туристы захотели перелет через Амстердам, находим нужную авиакомпанию “KLM + MARTINAIR”. В таблице “Расписание регулярных рейсов KLM (KL) + Martinair (MP) с пересадкой в Амстердаме (AMS) ” находим наш маршрут и его расписание: 11,10 в 4 день недели (четверг), Ш-2 – Амстердам и затем Амстердам-Пунта Канна.
  Ниже в  таблице “Стоимость авиабилетов, включая таксы и сборы” находим стоимость авиабилетов:
  Минимальный тариф на этом маршруте – 965 евро с человека. Условия - min правило воскр.= тур обязательно внутри себя должен содержать день недели воскресенье, т е если полет с понедельника по субботу – этот тариф не действует, max 1 месяц = максимальное количество пребывания в стране 1 месяц.  Оба условия у нас выполняются. Но для окончательного подтверждения тарифа необходимо сделать бронь и получить на нее подтверждение (мы все это делаем через систему бронирования авиакомпании и высылаем вам).
  Цена авиабилета указана за маршрут туда - обратно и включает: место в самолете, питание, разрешение на провоз определенного количества кг багажа (зависит от класса перелета и указано в авиабилете). Цена авиабилета указано в евро, но оплата будет производиться по курсу, который указан перед списком тарифов.
                            </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo21">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Инструкция по расчету сложных туров по суточным ценам (Доминикана)</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo21" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo31">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Правила расчета заявок при работе по системе тарифов</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo31" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo41">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Скачать иллюстрированную инструкцию по работе с главной страницей сайта</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo41" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo51">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Правила предварительного бронирования</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo51" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>


                    </div>
                    <!-- Перелет -->
                    <div class="faq-content" id="3" style="display:none;">
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo12">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Правила расчета заявок при работе по системе тарифов</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo12" class="collapse collapse-text">
                            <p>Тарифы и расписание авиперелетов на рейсах иностранных авиакомпаний указаны на сайте. На главной странице в крайней левой колонке на голубом фоне перечислены все страны под своими флагами, находим страну Доминикана, под названием страны находим ссылку “Авиа перелеты”, открываем ее, находим файл для загрузки “Стоимость и расписание регулярных рейсов на Пунта Кана”,  открываем его. Наши туристы захотели перелет через Амстердам, находим нужную авиакомпанию “KLM + MARTINAIR”. В таблице “Расписание регулярных рейсов KLM (KL) + Martinair (MP) с пересадкой в Амстердаме (AMS) ” находим наш маршрут и его расписание: 11,10 в 4 день недели (четверг), Ш-2 – Амстердам и затем Амстердам-Пунта Канна.
    Ниже в  таблице “Стоимость авиабилетов, включая таксы и сборы” находим стоимость авиабилетов:
    Минимальный тариф на этом маршруте – 965 евро с человека. Условия - min правило воскр.= тур обязательно внутри себя должен содержать день недели воскресенье, т е если полет с понедельника по субботу – этот тариф не действует, max 1 месяц = максимальное количество пребывания в стране 1 месяц.  Оба условия у нас выполняются. Но для окончательного подтверждения тарифа необходимо сделать бронь и получить на нее подтверждение (мы все это делаем через систему бронирования авиакомпании и высылаем вам).
    Цена авиабилета указана за маршрут туда - обратно и включает: место в самолете, питание, разрешение на провоз определенного количества кг багажа (зависит от класса перелета и указано в авиабилете). Цена авиабилета указано в евро, но оплата будет производиться по курсу, который указан перед списком тарифов.
                            </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo22">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Инструкция по расчету сложных туров по суточным ценам (Доминикана)</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo22" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo32">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Правила расчета заявок при работе по системе тарифов</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo32" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo42">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Скачать иллюстрированную инструкцию по работе с главной страницей сайта</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo42" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>


                    </div>
                    <!-- Бронирование и оплата -->
                    <div class="faq-content" id="4" style="display:none;">
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo33">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"> <span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Правила расчета заявок при работе по системе тарифов</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo33" class="collapse collapse-text">
                            <p>Тарифы и расписание авиперелетов на рейсах иностранных авиакомпаний указаны на сайте. На главной странице в крайней левой колонке на голубом фоне перечислены все страны под своими флагами, находим страну Доминикана, под названием страны находим ссылку “Авиа перелеты”, открываем ее, находим файл для загрузки “Стоимость и расписание регулярных рейсов на Пунта Кана”,  открываем его. Наши туристы захотели перелет через Амстердам, находим нужную авиакомпанию “KLM + MARTINAIR”. В таблице “Расписание регулярных рейсов KLM (KL) + Martinair (MP) с пересадкой в Амстердаме (AMS) ” находим наш маршрут и его расписание: 11,10 в 4 день недели (четверг), Ш-2 – Амстердам и затем Амстердам-Пунта Канна.
    Ниже в  таблице “Стоимость авиабилетов, включая таксы и сборы” находим стоимость авиабилетов:
    Минимальный тариф на этом маршруте – 965 евро с человека. Условия - min правило воскр.= тур обязательно внутри себя должен содержать день недели воскресенье, т е если полет с понедельника по субботу – этот тариф не действует, max 1 месяц = максимальное количество пребывания в стране 1 месяц.  Оба условия у нас выполняются. Но для окончательного подтверждения тарифа необходимо сделать бронь и получить на нее подтверждение (мы все это делаем через систему бронирования авиакомпании и высылаем вам).
    Цена авиабилета указана за маршрут туда - обратно и включает: место в самолете, питание, разрешение на провоз определенного количества кг багажа (зависит от класса перелета и указано в авиабилете). Цена авиабилета указано в евро, но оплата будет производиться по курсу, который указан перед списком тарифов.
                            </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo23">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Инструкция по расчету сложных туров по суточным ценам (Доминикана)</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo23" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>

                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo43">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Скачать иллюстрированную инструкцию по работе с главной страницей сайта</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo43" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>


                    </div>
                    <div class="faq-content" id="5" style="display:none;">
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo51">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Правила расчета заявок при работе по системе тарифов</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo51" class="collapse collapse-text">
                            <p>Тарифы и расписание авиперелетов на рейсах иностранных авиакомпаний указаны на сайте. На главной странице в крайней левой колонке на голубом фоне перечислены все страны под своими флагами, находим страну Доминикана, под названием страны находим ссылку “Авиа перелеты”, открываем ее, находим файл для загрузки “Стоимость и расписание регулярных рейсов на Пунта Кана”,  открываем его. Наши туристы захотели перелет через Амстердам, находим нужную авиакомпанию “KLM + MARTINAIR”. В таблице “Расписание регулярных рейсов KLM (KL) + Martinair (MP) с пересадкой в Амстердаме (AMS) ” находим наш маршрут и его расписание: 11,10 в 4 день недели (четверг), Ш-2 – Амстердам и затем Амстердам-Пунта Канна.
Ниже в  таблице “Стоимость авиабилетов, включая таксы и сборы” находим стоимость авиабилетов:
Минимальный тариф на этом маршруте – 965 евро с человека. Условия - min правило воскр.= тур обязательно внутри себя должен содержать день недели воскресенье, т е если полет с понедельника по субботу – этот тариф не действует, max 1 месяц = максимальное количество пребывания в стране 1 месяц.  Оба условия у нас выполняются. Но для окончательного подтверждения тарифа необходимо сделать бронь и получить на нее подтверждение (мы все это делаем через систему бронирования авиакомпании и высылаем вам).
Цена авиабилета указана за маршрут туда - обратно и включает: место в самолете, питание, разрешение на провоз определенного количества кг багажа (зависит от класса перелета и указано в авиабилете). Цена авиабилета указано в евро, но оплата будет производиться по курсу, который указан перед списком тарифов.
                            </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo52">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Инструкция по расчету сложных туров по суточным ценам (Доминикана)</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo52" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo53">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Правила расчета заявок при работе по системе тарифов</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo53" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo54">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Скачать иллюстрированную инструкцию по работе с главной страницей сайта</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo54" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo55">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Правила предварительного бронирования</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo55" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>
                      <div class="instruction-wrap" data-toggle="collapse" data-target="#demo56">
                          <div class="collapse-block" >
                            <p class="collapse-buttons"><span class="faq-content-active-span"><i class="fas fa-plus-circle"></i></span>Инструкция по расчету сложных туров по суточным ценам (Доминикана)</p>
                              <div class="collapse-arrow">
                                  <img src="../images/icons/collapse-arrow.png" alt="">
                              </div>
                          </div>
                          <div id="demo56" class="collapse collapse-text">
                            <p>Lorem ipsum dolor sit amet,  adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
                                  </div>
                      </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- end faq -->
    <?php include "layout/footer.php"; ?>
    <script type="text/javascript">
    $('.collapse-buttons').click(function(){
      console.log('asdd');
       $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle')
    });

    </script>
