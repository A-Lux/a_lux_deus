<?php include "layout/header.php"; ?>

    <section>
      <div class="container compare-tours">
          <div class="link">
            <span><a href="index.php"> Главная страница</a> - </span> <span> Сравнение туров</span>
          </div>
          <div class="title">
            <h1>Сравнение туров</h1>
          </div>
          <div class="col-sm-12">
            <div class="row">
            <div class="col-sm-3">
              <div class="card card-tour">
                <div class="card-img">
                  <img src="images/926308.png" alt="">
                </div>
                <div class="card-content">
                  <div class="tour-rating">
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="icon-star-empty"></span>
                  </div>
                  <div class="tour-title">
                    <h3>badawia resort, 3</h3>
                    <span>Египет, Шарм-эль-Шейх</span>
                  </div>
                  <div class="tour-about">
                    <ul>
                      <li>Вылет:<span>Алматы</span></li>
                      <li>Дата:<span>03.07</span></li>
                      <li>Дней:<span>8</span></li>
                      <li>Питание:<span>All</span></li>
                      <li>Перелет:<span>Эконом (2ч)</span></li>
                    </ul>
                  </div>
                  <div class="tour-prices">
                    <!-- <div class="tour-persons one-person">
                        <span class="person-price">Цена за 1 чел.</span>
                        <span class="tour-price">230 000 kzt</span>
                    </div> -->
                    <div class="tour-persons two-persons">
                        <span class="person-price">Цена за 2чел.</span>
                        <span class="tour-price">400 000 kzt</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="card card-tour">
                <div class="card-img">
                  <img src="images/2041321.jpg" alt="">
                </div>
                <div class="card-content">
                  <div class="tour-rating">
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="icon-star-empty"></span>
                  </div>
                  <div class="tour-title">
                    <h3>old vic resort, 4</h3>
                    <span>Египет, Шарм-эль-Шейх</span>
                  </div>
                  <div class="tour-about">
                    <ul>
                      <li>Вылет:<span>Алматы</span></li>
                      <li>Дата:<span>03.07</span></li>
                      <li>Дней:<span>8</span></li>
                      <li>Питание:<span>All</span></li>
                      <li>Перелет:<span>Эконом (2ч)</span></li>
                    </ul>
                  </div>
                  <div class="tour-prices">
                    <!-- <div class="tour-persons one-person">
                        <span class="person-price">Цена за 1 чел.</span>
                        <span class="tour-price">230 000 kzt</span>
                    </div> -->
                    <div class="tour-persons two-persons">
                        <span class="person-price">Цена за 2чел.</span>
                        <span class="tour-price">400 000 kzt</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="card card-tour">
                <div class="card-img">
                  <img src="images/Sharm_Inn_Amaren_9.jpg" alt="">
                </div>
                <div class="card-content">
                  <div class="tour-rating">
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="icon-star-empty"></span>
                  </div>
                  <div class="tour-title">
                    <h3>sharm inn, 5</h3>
                    <span>Египет, Шарм-эль-Шейх</span>
                  </div>
                  <div class="tour-about">
                    <ul>
                      <li>Вылет:<span>Алматы</span></li>
                      <li>Дата:<span>03.07</span></li>
                      <li>Дней:<span>8</span></li>
                      <li>Питание:<span>All</span></li>
                      <li>Перелет:<span>Эконом (2ч)</span></li>
                    </ul>
                  </div>
                  <div class="tour-prices">
                    <!-- <div class="tour-persons one-person">
                        <span class="person-price">Цена за 1 чел.</span>
                        <span class="tour-price">230 000 kzt</span>
                    </div> -->
                    <div class="tour-persons two-persons">
                        <span class="person-price">Цена за 2чел.</span>
                        <span class="tour-price">400 000 kzt</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="card card-tour">
                <div class="card-img">
                  <img src="images/4big.jpg" alt="">
                </div>
                <div class="card-content">
                  <div class="tour-rating">
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="icon-star-empty"></span>
                  </div>
                  <div class="tour-title">
                    <h3>badawia resort, 3</h3>
                    <span>Египет, Шарм-эль-Шейх</span>
                  </div>
                  <div class="tour-about">
                    <ul>
                      <li>Вылет:<span>Алматы</span></li>
                      <li>Дата:<span>03.07</span></li>
                      <li>Дней:<span>8</span></li>
                      <li>Питание:<span>All</span></li>
                      <li>Перелет:<span>Эконом (2ч)</span></li>
                    </ul>
                  </div>
                  <div class="tour-prices">
                    <!-- <div class="tour-persons one-person">
                        <span class="person-price">Цена за 1 чел.</span>
                        <span class="tour-price">230 000 kzt</span>
                      </div> -->
                      <div class="tour-persons two-persons">
                          <span class="person-price">Цена за 2чел.</span>
                          <span class="tour-price">400 000 kzt</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
    <?php include "layout/footer.php"; ?>
