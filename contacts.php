<?php include "layout/header.php"; ?>


    <section>
      <div class="container tour-contacts">
          <div class="link">
            <span><a href="index.php">Главная страница</a> - </span> <span> Контакты</span>
          </div>
          <div class="title">
            <h1>Контакты турагентства</h1>
          </div>
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-6">
                <div class="contact-phones">
                  <div class="row">
                <div class="city-phone phone-numbers">
                    <span>Городской:</span>
                    <p>8 (727) 648-78-87</p>
                  </div>
                  <div class="mobile-phone phone-numbers">
                    <span> Мобильный:</span>
                    <p>8 (727) 648-78-87</p>
                  </div>
                </div>
                </div>
                <div class="email-tour">
                  <div class="row">
                  <div class="emails">
                    <span> E-mail:</span>
                    <p>travelmarket@admin.ru</p>
                  </div>
                  <div class="social-netwroks">
                    <a href="">
                    <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                    </span>
                    </a>
                    <a href="">
                    <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-odnoklassniki fa-stack-1x fa-inverse"></i>
                    </span>
                    </a>
                    <a href="">
                    <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-vk fa-stack-1x fa-inverse"></i>
                    </span>
                    </a>
                    <a href="">
                    <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                    </span>
                    </a>
                    </div>
                  </div>
                </div>
                <div class="address-tour">
                  <div class="row">
                  <div class="address-place">
                    <span> Адреса в г.Алматы:</span>
                    <p>пр. Назарбаева, д.130</p>
                    <p>ул. Желтоксан, д.124</p>
                    <p> ул. Богенбай батыр, 81</p>
                  </div>
                  <div class="working-time">
                    <span>Часы работы</span>
                    <p>с 10:00 до 19:00</p>
                    <p>Суббота: с 10:00 до 15:00</p>
                    <p>Воскресенье: Выходной</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 nopadding">
                <p>По вопросам контроля качества обслуживания обращатся в
  администрацию, по телефону + 7 (705) 173-55-55 в будние дни,
  с 10:00 до 19:00.</p>
              </div>
            </div>

              <div class="col-sm-4 right-area">
                <img src="images/Sharm_Inn_Amaren_9.jpg" alt="" width="505px" height="440px">
              </div>

        </div>
            </div>
          </div>
        </div>
        <div class="map-yandex col-sm-12">
          <div id="map" style="width: 100%; height: 400px"></div>

  </div>
    </section>
    <script type="text/javascript">
 ymaps.ready(function () {
   var myMap = new ymaps.Map('map', {
     center: [43.2565, 76.9285],
           zoom:12
       }, {
           searchControlProvider: 'yandex#search'
       }),

       MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
           '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
            ),

       myPlacemarkWithContent = new ymaps.Placemark([43.2565, 76.9285], {

       }, {
           iconImageSize: [32, 32],
           iconImageOffset: [-12, -30],
           iconContentLayout: MyIconContentLayout
       });

   myMap.geoObjects
       .add(myPlacemarkWithContent);
});
   </script>

   <?php include "layout/footer.php"; ?>
