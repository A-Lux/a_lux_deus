<?php include "layout/header.php"; ?>

    <div class="main">
        <div class="container">
            <div class="blue-title show-all">
                <h1>Каталог туров</h1>
            </div>
            <div class="filter-row">
                <div class="filter-item tour">
                    <div class="image">
                        <img src="images/plane.png" alt="">
                    </div>
                    <p>Тур</p>
                </div>
                <div class="filter-item hot-tour">
                    <div class="image">
                        <img src="images/fire.png" alt="">
                    </div>
                    <p>Горящий тур</p>
                </div>
                <div class="filter-item cruise">
                    <div class="image">
                        <img src="images/anchor.png" alt="">
                    </div>
                    <p>Круиз</p>
                </div>
                <div class="filter-item made-kz">
                    <div class="image">
                        <img src="images/kz.png" alt="">
                    </div>
                    <p>Сделано в KZ</p>
                </div>
                <div class="filter-item promo">
                    <div class="image">
                        <img src="images/percent.png" alt="">
                    </div>
                    <p>Акции</p>
                </div>
                <div class="filter-item reviews">
                    <div class="image">
                        <img src="images/star-ico.png" alt="">
                    </div>
                    <p>Отзывы</p>
                </div>
                <div class="filter-item exclusive-tour">
                    <div class="image">
                        <img src="images/cam-ico.png" alt="">
                    </div>
                    <p>Экскурсионный тур</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="catalog-item promo-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/percent.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img1.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/percent.png" alt="">
                                        </div>
                                        <p>Акции</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="red-bg">Акции</span> Отели ОАЭ на "Все Включено"
                                    по акционным ценам! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item exclusive-tour-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/cam-ico.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img4.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/cam-ico.png" alt="">
                                        </div>
                                        <p>Экскурсионный тур</p>
                                    </div>
                                </div>

                                <div class="add-comp">
                                  <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="brown-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>Мальдивы</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item tour-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/plane.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/plane.png" alt="">
                                        </div>
                                        <p>Тур</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                  <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item cruise-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/anchor.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/anchor.png" alt="">
                                        </div>
                                        <p>Тур</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                  <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 ">
                    <div class="catalog-item review-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/star-ico.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <img src="images/img2.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/star-ico.png" alt="">
                                        </div>
                                        <p>Отзыв</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                  <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p>Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="purple-bg">Отзыв</span> Отзыв о Бодруме!</p>
                            </div>
                            <div class="country">
                                <p>Греция</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item tour-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/plane.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/plane.png" alt="">
                                        </div>
                                        <p>Тур</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                  <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item review-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/star-ico.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <img src="images/img2.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/star-ico.png" alt="">
                                        </div>
                                        <p>Отзыв</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                  <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p>Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="purple-bg">Отзыв</span> Отзыв о Бодруме!</p>
                            </div>
                            <div class="country">
                                <p>Греция</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item hot-tour-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/fire.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/fire.png" alt="">
                                        </div>
                                        <p>Горящий  Тур</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                  <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="orange-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item made-kz-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/kz.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/kz.png" alt="">
                                        </div>
                                        <p>Тур</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                  <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 ">
                    <div class="catalog-item exclusive-tour-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/cam-ico.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img3.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/cam-ico.png" alt="">
                                        </div>
                                        <p>Экскурсионный тур</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                  <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                               <div class="rating">
                                   <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                               </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="brown-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item hot-tour-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/fire.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/fire.png" alt="">
                                        </div>
                                        <p>Горящий  Тур</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                  <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="orange-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item promo-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/percent.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img1.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/percent.png" alt="">
                                        </div>
                                        <p>Акции</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                  <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="red-bg">Акции</span> Отели ОАЭ на "Все Включено"
                                    по акционным ценам! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                    <div class="catalog-item cruise-item">
                        <div class="image">
                            <div class="filter-wrap">
                                <div class="filter-item">
                                    <div class="image">
                                        <img src="images/anchor.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="price">
                                <p>230 000 KZT</p>
                            </div>
                            <img src="images/img5.jpg" class="img-fluid" alt="">
                            <div class="hover-overlay">
                                <div class="filter-wrap">
                                    <div class="filter-item">
                                        <div class="image">
                                            <img src="images/anchor.png" alt="">
                                        </div>
                                        <p>Тур</p>
                                    </div>
                                </div>
                                <div class="add-comp">
                                  <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                </div>
                                <div class="add-fav">
                                    <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                </div>
                                <div class="rating">
                                    <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                </div>
                                <div class="pros-list">
                                    <ul>
                                        <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                        <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                        <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                        <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                        <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                    </ul>
                                </div>
                                <div class="button-row">
                                    <button>Все продано</button>
                                    <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <div class="promo ">
                                <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                            </div>
                            <div class="country">
                                <p>ОАЭ</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="demo" class="collapse col-sm-12">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6" style="margin-top:-100px;">
                            <div class="catalog-item promo-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/percent.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img1.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/percent.png" alt="">
                                                </div>
                                                <p>Акции</p>
                                            </div>
                                        </div>
                                        <div class="add-comp">
                                          <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                        </div>
                                        <div class="add-fav">
                                            <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="red-bg">Акции</span> Отели ОАЭ на "Все Включено"
                                            по акционным ценам! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item exclusive-tour-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/cam-ico.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img4.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/cam-ico.png" alt="">
                                                </div>
                                                <p>Экскурсионный тур</p>
                                            </div>
                                        </div>
                                        <div class="add-comp">
                                          <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                        </div>
                                        <div class="add-fav">
                                            <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="brown-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>Мальдивы</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item tour-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/plane.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/plane.png" alt="">
                                                </div>
                                                <p>Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-comp">
                                          <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                        </div>
                                        <div class="add-fav">
                                            <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item cruise-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/anchor.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/anchor.png" alt="">
                                                </div>
                                                <p>Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-comp">
                                          <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                        </div>
                                        <div class="add-fav">
                                            <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 ">
                            <div class="catalog-item review-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/star-ico.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <img src="images/img2.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/star-ico.png" alt="">
                                                </div>
                                                <p>Отзыв</p>
                                            </div>
                                        </div>
                                        <div class="add-comp">
                                          <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                        </div>
                                        <div class="add-fav">
                                          <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="rating">
                                            <p>Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="purple-bg">Отзыв</span> Отзыв о Бодруме!</p>
                                    </div>
                                    <div class="country">
                                        <p>Греция</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item tour-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/plane.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/plane.png" alt="">
                                                </div>
                                                <p>Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-comp">
                                          <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                        </div>
                                        <div class="add-fav">
                                            <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item review-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/star-ico.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <img src="images/img2.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/star-ico.png" alt="">
                                                </div>
                                                <p>Отзыв</p>
                                            </div>
                                        </div>
                                        <div class="add-comp">
                                          <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                        </div>
                                        <div class="add-fav">
                                          <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="rating">
                                            <p>Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="purple-bg">Отзыв</span> Отзыв о Бодруме!</p>
                                    </div>
                                    <div class="country">
                                        <p>Греция</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item hot-tour-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/fire.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/fire.png" alt="">
                                                </div>
                                                <p>Горящий  Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-comp">
                                          <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                        </div>
                                        <div class="add-fav">
                                            <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="orange-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item made-kz-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/kz.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/kz.png" alt="">
                                                </div>
                                                <p>Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-comp">
                                          <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                        </div>
                                        <div class="add-fav">
                                            <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6" style="margin-top: -100px;">
                            <div class="catalog-item exclusive-tour-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/cam-ico.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img3.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/cam-ico.png" alt="">
                                                </div>
                                                <p>Экскурсионный тур</p>
                                            </div>
                                        </div>
                                        <div class="add-comp">
                                          <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                        </div>
                                        <div class="add-fav">
                                            <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="brown-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item hot-tour-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/fire.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/fire.png" alt="">
                                                </div>
                                                <p>Горящий  Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-comp">
                                          <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                        </div>
                                        <div class="add-fav">
                                            <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="orange-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item promo-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/percent.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img1.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/percent.png" alt="">
                                                </div>
                                                <p>Акции</p>
                                            </div>
                                        </div>
                                        <div class="add-comp">
                                          <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                        </div>
                                        <div class="add-fav">
                                            <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="red-bg">Акции</span> Отели ОАЭ на "Все Включено"
                                            по акционным ценам! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-item cruise-item">
                                <div class="image">
                                    <div class="filter-wrap">
                                        <div class="filter-item">
                                            <div class="image">
                                                <img src="images/anchor.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <p>230 000 KZT</p>
                                    </div>
                                    <img src="images/img5.jpg" class="img-fluid" alt="">
                                    <div class="hover-overlay">
                                        <div class="filter-wrap">
                                            <div class="filter-item">
                                                <div class="image">
                                                    <img src="images/anchor.png" alt="">
                                                </div>
                                                <p>Тур</p>
                                            </div>
                                        </div>
                                        <div class="add-comp">
                                          <a href="sravnenie.php"><i class="far fa-chart-bar"></i></a>
                                        </div>
                                        <div class="add-fav">
                                            <a href="lk-dews.php#tab-2"><i class="far fa-heart"></i></a>
                                        </div>
                                        <div class="rating">
                                            <p> Luxor Resort <img src="images/rating.png" alt=""></p>
                                        </div>
                                        <div class="pros-list">
                                            <ul>
                                                <li><div class="icon"><img src="images/icon1.png" alt=""></div>C 25.09.2019 - 7 дней</li>
                                                <li><div class="icon"><img src="images/icon2.png" alt=""></div>Вылет из Алматы</li>
                                                <li><div class="icon"><img src="images/icon3.png" alt=""></div>Завтрак</li>
                                                <li><div class="icon"><img src="images/icon4.png" alt=""></div>Количество экскурсий - 7</li>
                                                <li><div class="icon"><img src="images/icon5.png" alt=""></div>Транспорт включен (автобус)</li>
                                            </ul>
                                        </div>
                                        <div class="button-row">
                                            <button>Все продано</button>
                                            <button><a href="card-tour.php">Посмотреть тур!</a></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <div class="promo ">
                                        <p><span class="green-bg">Тур</span> Сказочные Мальдивы на "Все Включено"! Вылет из Алматы!</p>
                                    </div>
                                    <div class="country">
                                        <p>ОАЭ</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="show-more">
                        <button data-toggle="collapse" data-target="#demo">Показать еще</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "layout/footer.php"; ?>
